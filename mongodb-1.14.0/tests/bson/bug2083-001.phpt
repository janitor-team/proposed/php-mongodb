--TEST--
PHPC-2083: Enum classes cannot be instantiated via bsonUnserialize()
--SKIPIF--
<?php require __DIR__ . "/../utils/basic-skipif.inc"; ?>
<?php skip_if_php_version('<', '8.1'); ?>
--FILE--
<?php
require_once __DIR__ . "/../utils/basic.inc";

enum MyEnum : string implements MongoDB\BSON\Persistable {
    case MYNAME = 'myvalue';
 
    public function bsonSerialize() {
        var_dump($this);
        return (array)$this;
    }
 
    public function bsonUnserialize(array $data) {
        var_dump($data);
    }
}

$bson = MongoDB\BSON\fromPHP(MyEnum::MYNAME);

hex_dump($bson);

var_dump(MongoDB\BSON\toPHP($bson));

?>
===DONE===
<?php exit(0); ?>
--EXPECT--

===DONE===
